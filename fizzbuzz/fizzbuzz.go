package main

import "fmt"

func main() {
	const Fizz = "Fizz"
	const Buzz = "Buzz"
	for i := 1; i < 101; i++ {
		result := ""
		// divided by 3?
		if i%3 == 0 {
			result += Fizz
		}
		// divided by 5?
		if i%5 == 0 {
			result += Buzz
		}
		// divided by 3, 5 or 15?
		if result != "" {
			// print string
			fmt.Println(result)
		} else {
			// print number
			fmt.Println(i)
		}
	}
}
