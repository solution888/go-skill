# Gin API Demo

Gin API Demo is a sample project with MySQL integration using Gin and Gorm.

This project contains following features:
- REST API server with [Gin Framework](https://github.com/gin-gonic/gin)
- Database integration using [Gorm](http://gorm.io/)
- Logging using [Logrus](https://github.com/sirupsen/logrus)
- Go and MySQL installation via [Docker](https://docs.docker.com)


## Prerequisites
- [Docker](https://docs.docker.com/install/)
- [Docker Compose](https://docs.docker.com/compose/install/)
  
## Installation
Check out this project
```
$ git clone https://gitlab.com/jinchengbo/go-skill.git
```

## How to run
```
$ cd gin-api-demo
$ docker-compose up -d
```

## API examples
 - Create user
     
    POST http://localhost:8080/api/v1/users
    ```json
    {
	    "name": "user",
	    "password": "password",
	    "email": "user@mail.com"
    }
    ```
  - Update user
  
    PUT http://localhost:8080/api/v1/users/{id}
    ```json
    {
	    "name": "new user"
    }
    ```
  - Read all users
  
    GET http://localhost:8080/api/v1/users

  - Read a user
  
    GET http://localhost:8080/api/v1/users/{id}

  - Delete user
    
    DELETE http://localhost:8080/api/v1/users/{id}