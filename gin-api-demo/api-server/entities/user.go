package entities

// User model schema
type User struct {
	Model
	Username string `gorm:"type:varchar(30);not null;unique" json:"username,omitempty"`
	Email    string `gorm:"type:varchar(30);not null;default:''" json:"email,omitempty"`
	Password string `gorm:"type:varchar(64);not null;default:''" json:"password,omitempty"`
}
