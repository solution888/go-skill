module api-server

go 1.12

require (
	github.com/gin-gonic/gin v1.4.0
	github.com/go-sql-driver/mysql v1.4.1
	github.com/jinzhu/gorm v1.9.8
	github.com/kr/pretty v0.1.0 // indirect
	github.com/sirupsen/logrus v1.2.0
	golang.org/x/crypto v0.0.0-20190506204251-e1dfcc566284 // indirect
	gopkg.in/yaml.v2 v2.2.2
)
