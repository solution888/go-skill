package middlewares

import (
	"api-server/utils"

	"github.com/gin-gonic/gin"
	logrus "github.com/sirupsen/logrus"
)

// JSONLogMiddleware logs a gin HTTP request in JSON format, with some additional key/values
func JSONLogMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		// process request
		c.Next()

		entry := utils.Logger().WithFields(logrus.Fields{
			"client_ip": utils.GetClientIP(c),
			"method":    c.Request.Method,
			"path":      c.Request.RequestURI,
			"status":    c.Writer.Status(),
			"referrer":  c.Request.Referer(),
			"type":      "api",
		})

		if c.Writer.Status() >= 500 {
			entry.Error(c.Errors.String())
		} else {
			entry.Info("")
		}

	}
}
