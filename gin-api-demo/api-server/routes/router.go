package routes

import (
	"api-server/config"
	"api-server/controllers"
	"api-server/middlewares"

	"github.com/gin-gonic/gin"
)

// Init initiates routing
func Init() *gin.Engine {
	router := gin.Default()

	// set middlewares
	if config.Get().Production == true {
		router.Use(middlewares.JSONLogMiddleware())
	}
	router.Use(gin.Recovery())

	v1Api := router.Group("/api/v1")
	{
		userController := new(controllers.User)
		v1Api.GET("/users", userController.Index)
		v1Api.GET("/users/:id", userController.GetByID)
		v1Api.POST("/users", userController.Create)
		v1Api.PUT("/users/:id", userController.Update)
		v1Api.DELETE("/users/:id", userController.Delete)
	}

	return router
}
