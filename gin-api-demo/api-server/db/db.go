package db

import (
	// MySql driver
	_ "github.com/go-sql-driver/mysql"

	"github.com/jinzhu/gorm"

	"api-server/config"

	"api-server/entities"
)

var (
	db  *gorm.DB
	err error
)

// Connect opens db connection
func Connect() (*gorm.DB, error) {
	conf := config.Get()

	db, err = gorm.Open("mysql", conf.DSN)
	// open failed?
	if err != nil {
		return nil, err
	}

	// successfully opened
	db.DB().SetMaxIdleConns(conf.MaxIdleConn)
	db.AutoMigrate(&entities.User{})
	return db, err
}

// GetDB returns db instance
func GetDB() *gorm.DB {
	return db
}
