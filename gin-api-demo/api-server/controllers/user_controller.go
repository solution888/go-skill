package controllers

import (
	"api-server/db"
	"api-server/entities"
	"crypto/md5"
	"encoding/hex"
	"net/http"

	"github.com/gin-gonic/gin"
)

// User is controller
type User struct {
	Basic
}

// Index action: GET /users
func (a *User) Index(c *gin.Context) {
	var user []entities.User

	db.GetDB().Select("id, username, email, created_at, updated_at").Order("id").Find(&user)
	a.JSONSuccess(c, http.StatusOK, gin.H{"data": user})
}

// GetByID action: GET /users/:id
func (a *User) GetByID(c *gin.Context) {
	var user entities.User
	if db.GetDB().Select("id, username, email, created_at, updated_at").First(&user, c.Param("id")).Error != nil {
		a.JSONFail(c, http.StatusNotFound, "User not found")
		return
	}
	a.JSONSuccess(c, http.StatusOK, gin.H{"data": user})
}

// Create action: POST /users
func (a *User) Create(c *gin.Context) {
	var params entities.User

	if err := c.ShouldBind(&params); err == nil {
		var count int
		db.GetDB().Model(entities.User{}).Where("username = ?", params.Username).Count(&count)

		if count > 0 {
			// already registered
			a.JSONFail(c, http.StatusBadRequest, "Username is in use")
			return
		}

		// create

		// encrypting password
		password := []byte(params.Password)
		md5Ctx := md5.New()
		md5Ctx.Write(password)
		cipherStr := md5Ctx.Sum(nil)
		user := entities.User{
			Username: params.Username,
			Email:    params.Email,
			Password: hex.EncodeToString(cipherStr),
		}

		// store
		if err := db.GetDB().Create(&user).Error; err != nil {
			a.JSONFail(c, http.StatusBadRequest, err.Error())
			return
		}
		a.JSONSuccess(c, http.StatusCreated, gin.H{"message": "created"})
	} else {
		a.JSONFail(c, http.StatusBadRequest, err.Error())
	}
}

// Update action: PUT /users/:id
func (a *User) Update(c *gin.Context) {
	var params entities.User

	if err := c.ShouldBind(&params); err == nil {
		var user entities.User

		if db.GetDB().First(&user, c.Param("id")).Error != nil {
			// user not found
			a.JSONFail(c, http.StatusNotFound, "User not found")
			return
		}

		// update
		user.Username = params.Username

		// store
		if err := db.GetDB().Save(&user).Error; err != nil {
			a.JSONFail(c, http.StatusBadRequest, err.Error())
			return
		}
		a.JSONSuccess(c, http.StatusOK, gin.H{"message": "updated"})

	} else {
		a.JSONFail(c, http.StatusBadRequest, err.Error())
	}
}

// Delete action: DELETE /users/:id
func (a *User) Delete(c *gin.Context) {
	var user entities.User

	if db.GetDB().First(&user, c.Param("id")).Error != nil {
		a.JSONFail(c, http.StatusNotFound, "User not found")
		return
	}

	// delete
	if err := db.GetDB().Unscoped().Delete(&user).Error; err != nil {
		a.JSONFail(c, http.StatusBadRequest, err.Error())
		return
	}
	a.JSONSuccess(c, http.StatusOK, gin.H{})
}
