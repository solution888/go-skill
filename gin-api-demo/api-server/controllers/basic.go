package controllers

import (
	"github.com/gin-gonic/gin"
)

// Basic returns json data
type Basic struct {
}

// JSONSuccess returns json data for success
func (basic *Basic) JSONSuccess(c *gin.Context, status int, h gin.H) {
	h["status"] = "success"
	c.JSON(status, h)
	return
}

// JSONFail returns json data for error
func (basic *Basic) JSONFail(c *gin.Context, status int, message string) {
	c.JSON(status, gin.H{
		"status":  "fail",
		"message": message,
	})
	return
}
