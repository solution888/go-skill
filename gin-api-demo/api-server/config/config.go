package config

import (
	"io/ioutil"

	"gopkg.in/yaml.v2"
)

// Config defines app config data
type Config struct {
	Addr        string `yaml:"addr"`
	DSN         string `yaml:"dsn"`
	MaxIdleConn int    `yaml:"max_idle_conn"`
	Production  bool   `yaml:"production"`
}

var config *Config

// Load reads yaml config file from path
func Load(path string) error {
	result, err := ioutil.ReadFile(path)
	if err != nil {
		return err
	}

	return yaml.Unmarshal(result, &config)
}

// Get returns config loaded
func Get() *Config {
	return config
}
