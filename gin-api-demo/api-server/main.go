package main

import (
	"api-server/config"
	"api-server/db"
	"api-server/routes"
	"api-server/utils"

	"github.com/sirupsen/logrus"

	"github.com/gin-gonic/gin"
)

func main() {
	// load config
	if err := config.Load("config/config.yaml"); err != nil {
		utils.Logger().WithFields(logrus.Fields{"type": "app"}).Error("Failed to load config file")
		return
	}

	// init utils
	if config.Get().Production == true {
		// logging to file in production
		utils.Init()
	}
	// connect db
	db, err := db.Connect()
	if err != nil {
		utils.Logger().WithFields(logrus.Fields{"type": "app"}).Error("Failed to open database")
		return
	}
	defer db.Close()

	if config.Get().Production == true {
		gin.SetMode(gin.ReleaseMode)
	}

	// run server
	router := routes.Init()

	if err := router.Run(config.Get().Addr).Error(); err != "" {
		utils.Logger().WithFields(logrus.Fields{"type": "app"}).Error(err)
	}

}
