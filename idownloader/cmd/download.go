package cmd

import (
	"fmt"
	"strings"

	"os"

	"time"

	grab "github.com/cavaliercoder/grab"
	"github.com/spf13/cobra"
)

// downloadCmd represents the download command
var downloadCmd = &cobra.Command{
	Use:   "download",
	Short: "Download file from url",
	Long: `This command downloads a file from url.
Please set the file url with '--input' or '-i' flag.`,
	Run: func(cmd *cobra.Command, args []string) {
		// parse flags
		url, _ := cmd.Flags().GetString("input")
		if url == "" {
			fmt.Println("Please set url with --input flag")
			os.Exit(1)
			return
		}

		// create client
		client := grab.NewClient()
		req, _ := grab.NewRequest(".", url)
		// start file download
		fmt.Printf("Download started\n")
		resp := client.Do(req)

		// start UI loop
		t := time.NewTicker(500 * time.Millisecond)
		defer t.Stop()

	Loop:
		for {
			select {
			case <-t.C:
				fmt.Printf("\r%s", strings.Repeat(" ", 80))
				fmt.Printf("\r Downloading... %v / %v bytes (%.2f%%)",
					resp.BytesComplete(),
					resp.Size,
					100*resp.Progress())
			case <-resp.Done:
				// download is complete
				fmt.Printf("\r%s", strings.Repeat(" ", 80))
				break Loop
			}
		}

		// check for errors
		if err := resp.Err(); err != nil {
			fmt.Fprintf(os.Stderr, "\n Download failed: %v\n", err)
			os.Exit(1)
		}

		fmt.Printf("\rSuccessfully downloaded to .\\%v \n", resp.Filename)
	},
}

func init() {
	rootCmd.AddCommand(downloadCmd)	
	downloadCmd.Flags().StringP("input", "i", "", "set url for file to download")
}
