# **IDownloader**

IDownloader is a simple CLI program to download file from url, built with [Cobra](https://github.com/spf13/cobra) and [grab](https://github.com/cavaliercoder/grab).

Cobra is both a library for creating powerful modern CLI applications as well as a program to generate applications and command files.

----

## Commands

- Help

```
  $idownloader help  
```

- Download
``` 
  $idownloader download --help
  $idownloader download --input url
  $idownloader download -i url
```

## Examples

- Download sample video files

```
  $idownloader download -i http://techslides.com/demos/sample-videos/small.webm
  $idownloader download -i http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4
```

- Download sample PDF files
```
  $idownloader download -i http://www.pdfpdf.com/samples/Sample1.PDF
```

## How to build

- Please clone this repository to your local workspace first.
```
  $git clone https://gitlab.com/jinchengbo/go-skill.git  
```
- Install dependencies.
```
  $go get ./...
```
- Run go build
